<?php

function get_db_conf(){
	return require 'db.php';
}

$conf = get_db_conf();
try {
    $pdo = new PDO($conf['dsn'], $conf['username'], $conf['password']);
} catch (PDOException $e) {
    die('Подключение не удалось: ' . $e->getMessage());
}
?>