<script src="http://code.jquery.com/jquery-latest.js"></script>
<style>
    ::-webkit-scrollbar
    {
        width: 20px;  /* for vertical scrollbars */
        height: 12px; /* for horizontal scrollbars */
    }

    ::-webkit-scrollbar-track
    {
        width:50px;
        background: rgba(0, 0, 0, 0.1);

    }

    ::-webkit-scrollbar-thumb
    {
        border-radius: 10px;
        background: rgba(127, 101, 117, 0.29);
    }


    .block {
        width: 480px;
        height: 200px;
        background-color: #F1F1F1;
        margin-top: 20px;
        margin-left: 10px;
        border-radius: 30px;
    }

    .mess{
        /*   display:none; */
    }

    .main{
        height:800px;
        overflow-y: scroll;
        width:520px;
        margin: 0 auto;
        background-color: #E1E1E8;
    }

    .block:last-child{
        display:none;
    }

    button {
        width: 150px;
        height: 60px;
        box-shadow: 0px 0px 75px 0px #6F7071 inset,0px 0px 75px 0px #767C96;;
        background-color: #441D32;
        color: #A99D9D;
        border: 1px solid #736363;
    }


    .sub-block{
        width: 120px;
        height: 120px;
        display: block;
        background-color: #503745;
        position: relative;
        top: 30px;
        left: 15px;
        border-radius: 50%;
        background: url(../img/4304fd8dfefd904120be967dee6fbcc011-resize-2000x2000-same-d4e268.jpg) no-repeat center;
        background-size: cover;
    }

    .block-control{
        position: fixed;
        right: 50px;
        top: 50px;
    }
</style>

<script>
    $(document).ready(function() {

        $("button").click(function() {
            $("<div class='block'><div class='sub-block'></div></div>").appendTo(".mess")
            $(".block:last").fadeIn(700);
            var height = $(".mess").height();
            $(".main").animate({"scrollTop":height},500);
            // $(".block").stop();
            // $(".block").fadeOut(700);
            // $(".block:last-child").fadeIn(700);
        });
    });
</script>



<div class="main">
    <div class="mess">
    </div>
</div>
<div class="block-control">
    <button>Add block</button>
</div>


