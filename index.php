<?php
require 'user.php';
require 'config.php' ;
?>

<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Basic Bootstrap Template</title>      
    <!-- 1. Подключаем скомпилированный и минимизированный файл CSS Bootstrap 3 -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/main_style.css" rel="stylesheet">
	<script src="http://code.jquery.com/jquery-latest.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
	<script src="/ajaxupload.3.5.js"></script>
	<script>
		function send_message() {
		 	// var str = $('#send-messege').val();
		 	var messege = $('#send-messege').val();
		 	$('#send-messege').val("");
			if (messege != '') {
                $.ajax({
			      url: "send_mess.php",
			      // global: false,
			       type: "post",
			       data: ({messege : messege}),
			       dataType: "html",
			       success: function(msg){
			      	// alert(msg);
				      	if (msg == true){
				         $('.mess-stat span').text( msg );
							$(".mess-stat span").fadeIn(900);
							$(".mess-stat span").fadeOut(1800);
					    }else{
				          $('.mess-stat span').text( msg );
							$(".mess-stat span").fadeIn(900);
							$(".mess-stat span").fadeOut(1800);
					    }
				    }
				});
			}
		 }
	    var last = 'empty';
	  	function getmsg() {
            $.ajax({
		      url: "messeges.php",
		      type: "post",
		      data: ({getmsg : last}),
		      success: function(msg) {
		      	if (msg) {
			        var cart = JSON.parse(msg);
				    for (var i = 0;i < cart.length;i++) {
						if(cart[i]['user_id'] == '.block-main'){
							$('#mess').append('<div class="block-main"><div class="mess-text"><span class="name">' + cart[i].name  + ' </span><span class="time">'+ cart[i].time +'</span><p mess-id="' + cart[i].id + '" class="mess">'+ cart[i].message + '</p></div><div class="sub-block" style="background: url(../img/'+ cart[i].user_img +') no-repeat center; background-size: cover;"></div></div>');
						}else{
							$('#mess').append('<div class="block"><div class="sub-block" style="background: url(../img/'+ cart[i].user_img +') no-repeat center; background-size: cover;"></div><div class="mess-text"><span class="name">' + cart[i].name  + ' </span><span class="time">'+ cart[i].time +'</span><p mess-id="' + cart[i].id + '" class="mess">'+ cart[i].message + '</p></div></div>');
						}
						$(".block:last").fadeIn(700);
						var height = $("#mess").height();

						if(cart.length > 20){
							$(".main").animate({"scrollTop":height},0);
						}else{
							$(".main").animate({"scrollTop":height},200);
						}
					}
				      	last = $('div p:last').attr('mess-id');
			    	}
					getmsg();
		    	}
			});
		}
        function get_online_users(){
			$.ajax({
		      url: "on_users.php",
		      // global: false,
		       type: "post",
		       data: ({show_on : 1}),
		       dataType: "html",
		       success: function(users){
		       	    var user = JSON.parse(users);
                    $('.us-online').empty();   
                    for (var i = 0;i < user.length;i++) {          		
				       	$('.us-online').append('<div class="online-us">' + user[i].name + '</div>');
				       	$('.us-online').fadeIn(900);

			       	}
			    }
			});
	    }
        get_online_users();
	  	getmsg();
		$(document).ready(function() {
	     	$("#send-messege").keypress(function(e){
	     	   if(e.keyCode==13){
	     	   		send_message();
	     	   }
	     	});
	     	$(".block-users-online").click(function(e){
	             get_online_users();
	     	});
		   $("span.glyphicon.glyphicon-align-justify.menu-on").click(function(){   
		   	    $(".menu-on").stop();
		   	    if($(".block-users-online").css('left') == '-200px'){
			        $(".block-users-online").animate({
			        left: "0px",
			        }, 500 );
				}else{
			        $(".block-users-online").animate({
			        left: "-200px",
			        }, 500 );
				}
		    });
		   var height;
		   $("div.main").scroll(function(){
			    height = $("div.main").scrollTop();
				if (height < 300) {
			      alert(height);
			    }
			});

		   
// function SetTitle() {
// var a = 1;
// setInterval (function() {
//   if (a == 1) {
// $("title").text('Новое сообщение');
//     a = 0;
//   }else{
//  $("title").text('Диалог');
//     a = 1;
//   }
// },1000);
// }

		});
	</script>
	</head>
	     <body>
	     	<div class="block-users-online" style="left: 0px;">
			<button>online<span class="glyphicon glyphicon-phone"></span></button>
			<div class="us-online"><div class="online-us">Slavik</div><div class="online-us">Anya</div><div class="online-us">Andrey</div></div>
			<div class="col g-7">
                <ul class="copyright">
                   <li>Developed by <a href="#" title="Styleshout">Visotsky V.S. </a></li>      
                   <li>2016</li>         
                </ul>
             </div>
		    </div>
	     	<div class="main-container col-lg-6 col-lg-offset-3">
	     		<div class="header">
	     			<span class="glyphicon glyphicon-align-justify menu-on"></span>
	     			<a class="sv-label" href="/"><img src="img/image1.png"></a>
	     			<div class="info">
					 <?php
					$auth = new AuthClass($pdo);
					if (isset($_POST["login"]) && isset($_POST["password"])) { 
					    if (!$auth->auth($_POST["login"], $_POST["password"])) { 
					        echo "<h2 style='color:red;'>Логин и пароль введен не правильно!</h2>";
					    }
					}
					if (isset($_GET["is_exit"])) { 
					    if ($_GET["is_exit"] == 1) {
					        $auth->out(); //Выходим
//					        header("Location: ?is_exit=0");
					    }
					}

					if ($auth->isAuth()) { 
					    echo "<span>(" . $auth->getLogin() . ')</span>';
					    echo " <a href='?is_exit=1'>Выйти <span class='glyphicon glyphicon-log-in'></span></a>"; 

					?>
					</div>
				</div>
				<div class="main">
					<div id="mess">
					</div>
				</div>


				<form  class="send-mess-form" method="post" enctype=multipart/form-data>
					<textarea id="send-messege" rows="5" cols="45" name="text"></textarea>
					<div class="btn btn-primary send-mess" onclick="send_message()" onkeydown="send_message()" >Отправить</div>
					<div class="mess-stat"><span></span></div>
					<!-- <a class="Upload"  id="Upload">Прикрепить</a> -->
					<!-- <div id="InfoBox"></div> -->
				</form>
		</div>
<!-- 		<div class="block-users-online">
			<button>online<span class="glyphicon glyphicon-phone"></span></button>
			<div class="us-online"></div>
			<div class="col g-7">
                <ul class="copyright">
                   <li>Developed by <a href="#" title="Styleshout">Visotsky V.S. </a></li>      
                   <li>2016</li>         
                </ul>
             </div>
		</div> -->
				<?php


				}

				else {
				?>
                <div class="login-form">
                    <form method="post" action="index.php">
                        <div class="input-group"><input class="form-control"  placeholder="Username" type="text" name="login" value="<?php echo (isset($_POST["login"])) ? $_POST["login"] : null; // Заполняем поле по умолчанию ?>" /></div>
                        <div class="input-group"><input class="form-control" placeholder="Password" type="password" name="password" value="" /></div>
                        <input class="btn btn-primary" type="submit" value="Войти" /></div>
                    </form>
                </div>
				<?php }
			?>
			<div id="triangle-bottomleft"></div>
			<div id="box-with-shadow-one"></div>
			<div id="box-with-shadow-two"></div>
		 </body>
</html>