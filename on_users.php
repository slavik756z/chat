<?php 

include ('config.php');

// SELECT online,name from users where online = 1;
if ($_POST['show_on'] == 1) {
	$query = $pdo->prepare("SELECT name from users where online = 1");
	$query->execute();
	$online_users = $query->fetchAll(PDO::FETCH_ASSOC);
	echo json_encode($online_users , JSON_UNESCAPED_UNICODE);
}
