<?php
session_start();
session_id();


class AuthClass {

//    private $_login = "admin"; //Устанавливаем логин
//    private $_password = "admin"; //Устанавливаем пароль
//    private $_id = 1;
    public $pdo;


    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    public function isAuth() {
        if (isset($_SESSION["is_auth"])) { 
            return $_SESSION["is_auth"]; 
        }
        else return false; 
    }
     
    /**
     * Авторизация пользователя
     * @param string $login
     * @param string $passwors 
     */
    public function auth($login, $password) {
        $query = $this->pdo->prepare("SELECT * from users WHERE name ='". $login ."' AND password ='". $password ."'");
        $query->execute();
        $user = $query->fetchAll(PDO::FETCH_ASSOC);
        if ($user[0]['name']) {
            $_SESSION["is_auth"] = true;
            $_SESSION["login"] = $user[0]['name'];
            $_SESSION["id"] = $user[0]['id'];
            $this->pdo->prepare("UPDATE users SET online = 1 WHERE id = " .  $_SESSION["id"])->execute();
            return true;
        }
        else { //Логин и пароль не подошел
            $_SESSION["is_auth"] = false;
            return false;
        }
    }
     
    /**
     * Метод возвращает логин авторизованного пользователя 
     */
    public function getLogin() {
        if ($this->isAuth()) { 
            return $_SESSION["login"]; 
        }
    }
     
     
    public function out() {
        $this->pdo->prepare("UPDATE users SET online = 0 WHERE id = " .  $_SESSION["id"])->execute();
        $_SESSION = array(); 
        session_destroy(); 
    }
}